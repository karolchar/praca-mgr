from django import forms
from scheduleItems.models import ScheduleItem


class ScheduleItemCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        current_horse = kwargs.pop('horse')
        super().__init__(*args, **kwargs)
        self.fields['horse'].initial = current_horse

    class Meta:
        model = ScheduleItem
        fields = '__all__'
        widgets = {
            'dueDate': forms.widgets.DateInput(attrs={'type': 'date'}),
            'dueTime': forms.widgets.TimeInput(attrs={'type': 'time'}),
        }
