from django.db import models
from horses.models import Horses


class ScheduleItem(models.Model):
    title = models.CharField(blank=False, null=False, max_length=100)
    dueDate = models.DateField(blank=False, null=False)
    dueTime = models.TimeField(blank=False, null=False, default="12:00:00")
    description = models.CharField(max_length=255)
    horse = models.ForeignKey(
        Horses,
        on_delete=models.CASCADE,
        related_name='schedule_item',
        blank=True,
        null=True
        )

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "schedule item"
        verbose_name_plural = "schedule items"
