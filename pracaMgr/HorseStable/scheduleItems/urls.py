from django.urls import path
from .views import create_schedule_item, get_schedule_item
app_name = 'schedule_items_url'

urlpatterns = [
    path('create-scheduleitem/<int:horse_id>', create_schedule_item, name='create_schedule_item'),
    path('scheduleitem/<int:schedule_item_id>', get_schedule_item, name='get_schedule_item')
]