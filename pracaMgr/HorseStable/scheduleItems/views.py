from django.http import HttpResponse
from django.shortcuts import render, redirect

from horses.models import Horses
from scheduleItems.forms import ScheduleItemCreationForm
from django.contrib.auth.decorators import login_required
from scheduleItems.models import ScheduleItem


@login_required()
def get_schedule_item(request, schedule_item_id):
    try:
        schedule_item = ScheduleItem.objects.get(id=schedule_item_id)
    except ScheduleItem.DoesNotExist:
        return HttpResponse('Wydarzenie nie istnieje')
    return render(request, 'schedule_item_details.html', {'schedule_item': schedule_item})


@login_required()
def create_schedule_item(request, horse_id):
    horse = Horses.objects.get(id=horse_id)
    form = ScheduleItemCreationForm(horse=horse)
    if request.method == 'POST':
        form = ScheduleItemCreationForm(request.POST or None, horse=horse)
        if form.is_valid():
            form.save()
            return redirect('horses_url:view_horse')
        else:
            return render(request, 'crud_si/create.html', {'form': form})
    else:
        return render(request, 'crud_si/create.html', {'form': form, 'horse': horse})
