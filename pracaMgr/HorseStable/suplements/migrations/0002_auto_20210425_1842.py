# Generated by Django 3.1.4 on 2021-04-25 16:42

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('suplements', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='suplements',
            options={'verbose_name': 'suplement', 'verbose_name_plural': 'suplements'},
        ),
    ]
