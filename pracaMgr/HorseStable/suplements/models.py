from django.db import models


# Create your models here.


class Suplements(models.Model):
    suplement_name = models.CharField(max_length=50, blank=True)

    def __str__(self):
        return self.suplement_name

    class Meta:
        verbose_name = "suplement"
        verbose_name_plural = "suplements"
