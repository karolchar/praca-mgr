from django.urls import path
from .views import create_health_card, get_health_card, create_health_card_record

app_name = 'health_card_urls'

urlpatterns = [
    path('create-health-card/<int:horse_id>', create_health_card, name='create_health_card'),
    path('<int:healthcard_id>/', get_health_card, name='get_health_card'),
    path('health-card-<int:horse_id>/create-record', create_health_card_record, name='create_healthcard_record')
]
