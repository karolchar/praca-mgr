from django.db import models
from django.utils.translation import ugettext_lazy as _
from horses.models import Horses


class HealthCard(models.Model):
    horse = models.OneToOneField(Horses, on_delete=models.CASCADE, blank=False, null=True, related_name='health_card', default=None)

    def __str__(self):
        return self.horse.name

    class Meta:
        verbose_name = 'health card'
        verbose_name_plural = 'health cards'


class HealthCardRecord(models.Model):
    VACCINATION = 'VCC'
    NOURISHMENT = 'NRH'
    MEDICINE = 'MDC'
    HCTYPE_CHOICES = (
        (VACCINATION, _('Vaccination')),
        (NOURISHMENT, _('Nourishment')),
        (MEDICINE, _('Medicine'))
    )

    description = models.CharField(max_length=100, blank=False, null=False)
    date = models.DateField(blank=False, null=False)
    type = models.CharField(choices=HCTYPE_CHOICES, max_length=3)
    health_card = models.ForeignKey(HealthCard, on_delete=models.CASCADE, related_name='health_card_record')

    def __str__(self):
        return f'{self.type} | {self.description}'

    class Meta:
        verbose_name = 'health card record'
        verbose_name_plural = 'health card records'
