from django import forms
from health_card.models import HealthCard, HealthCardRecord


class HealthCardCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        actual_horse_id = kwargs.pop('horse_id')
        super().__init__()
        self.fields['horse'].initial = actual_horse_id

    class Meta:
        model = HealthCard
        fields = '__all__'


class HealthCardRecordCreationForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        actual_horse_health_card = kwargs.pop('health_card')
        super().__init__(*args, **kwargs)
        self.fields['health_card'].initial = actual_horse_health_card

    class Meta:
        model = HealthCardRecord
        fields = '__all__'
