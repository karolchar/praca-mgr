from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse
from .models import HealthCard, HealthCardRecord
from django.utils.translation import ugettext_lazy as _
from horses.models import Horses
from health_card.forms import HealthCardCreationForm, HealthCardRecordCreationForm


def get_health_card(request, healthcard_id):
    horse_hc = Horses.objects.get(health_card=healthcard_id)
    try:
        health_card = HealthCard.objects.get(id=healthcard_id)
        health_card_records = HealthCardRecord.objects.filter(health_card=health_card)
    except HealthCard.DoesNotExist:
        return HttpResponse(_('Karta zdrowia nie istnieje'))
    return render(request, 'crud/health_card.html', {'health_card': horse_hc,
                                                     'health_card_records': health_card_records})


def create_health_card(request, horse_id):
    #TODO czemu nie działaaaaaaa
    form = HealthCardCreationForm(horse_id=horse_id)
    if request.method == 'POST':
        form = HealthCardCreationForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect(reverse('get_health_card'), kwargs={'healthcard_id': form['horse'].health_card})
        else:
            return render(request, 'crud/create_health_card.html', {'form': form})
    else:
        return render(request, 'crud/create_health_card.html', {'form': form})


def create_health_card_record(request, horse_id):
    health_card = HealthCard.objects.get(horse=horse_id)
    form = HealthCardRecordCreationForm(health_card=health_card)
    if request.method == 'POST':
        form = HealthCardRecordCreationForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect(reverse('health_card_urls:get_health_card', kwargs={'healthcard_id': health_card.id}))
        else:
            return render(request, 'crud/create_healthcard_record.html', {'form': form})
    else:
        return render(request, 'crud/create_healthcard_record.html', {'form': form, 'health_card': health_card})
