# Generated by Django 3.1.4 on 2022-03-23 17:36

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('horses', '0009_auto_20220322_1827'),
    ]

    operations = [
        migrations.CreateModel(
            name='HealthCard',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('horse', models.OneToOneField(default=None, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='health_card', to='horses.horses')),
            ],
            options={
                'verbose_name': 'health card',
                'verbose_name_plural': 'health cards',
            },
        ),
        migrations.CreateModel(
            name='HealthCardRecord',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('description', models.CharField(max_length=100)),
                ('date', models.DateField()),
                ('type', models.CharField(choices=[('VCC', 'Vaccination'), ('NRH', 'Nourishment'), ('MDC', 'Medicine')], max_length=3)),
                ('health_card', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='health_card_record', to='health_card.healthcard')),
            ],
            options={
                'verbose_name': 'health card record',
                'verbose_name_plural': 'health card records',
            },
        ),
    ]
