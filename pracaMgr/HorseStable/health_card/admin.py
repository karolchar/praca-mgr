from django.contrib import admin
from .models import HealthCard, HealthCardRecord
# Register your models here.

admin.site.register(HealthCard)
admin.site.register(HealthCardRecord)

