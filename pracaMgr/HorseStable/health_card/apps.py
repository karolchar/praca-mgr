from django.apps import AppConfig


class HealthCardConfig(AppConfig):
    name = 'health_card'
    verbose_name = 'Health cards'
