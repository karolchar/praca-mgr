from django.shortcuts import render, redirect
from .forms import RegisterForm


# Create your views here.
def register_user(request):
    form = RegisterForm()
    if request.method == 'POST':
        form = RegisterForm(request.POST or None)
        if form.is_valid():
            form.save()
            return redirect('horses_url:view_horse')
        else:
            return render(request, 'registration/sign_up.html', {'form': form})
    else:
        return render(request, 'registration/sign_up.html', {'form': form})
