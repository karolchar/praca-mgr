from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from users.forms import CustomUserCreationForm, CustomUserChangeForm
from users.models import CustomUser


# Register your models here.

class CustomUserAdmin(UserAdmin):
    form = CustomUserChangeForm
    add_form = CustomUserCreationForm
    model = CustomUser
    list_display = ('email', 'first_name', 'last_name', 'role', 'is_active', 'is_admin')
    list_filter = ('first_name', 'last_name', 'role', 'is_active', 'is_admin')
    fieldsets = (
        (None, {'fields': ('email', 'password',)}),
        ('Personal info', {'fields': ('first_name', 'last_name', 'role',)}),
        ('Permissions', {'fields': ('is_admin', 'is_active', 'is_staff')})
    )
    add_fieldsets = (
        (
            None, {
                'classes': ('wide',),
                'fields': (
                    'email', 'first_name', 'last_name', 'role', 'password1', 'password2', 'is_staff', 'is_active',
                    'is_admin',)
            }
        ),
    )
    search_fields = ('email', 'first_name__exact', 'last_name__exact',)
    ordering = ('first_name', 'last_name',)


admin.site.register(CustomUser, CustomUserAdmin)
