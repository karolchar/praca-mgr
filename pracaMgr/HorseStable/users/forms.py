from django import forms
from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from django.utils.translation import ugettext_lazy as _
from users.models import CustomUser

User = get_user_model()


class RegisterForm(forms.ModelForm):
    password = forms.CharField(label=_('Password'), widget=forms.PasswordInput)
    password_2 = forms.CharField(label=_('Confirm password'), widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = ['email', 'first_name', 'last_name']

    def clean_email(self):
        email = self.cleaned_data.get('email')
        user_qs = User.objects.filter(email=email)
        if user_qs.exists():
            raise forms.ValidationError(_('Taki email już istnieje'))
        return email

    def clean(self):
        cleaned_data = super().clean()
        password = cleaned_data.get("password")
        password_2 = cleaned_data.get("password_2")
        if password is not None and password != password_2:
            self.add_error("password_2", _("Hasła nie są identyczne"))
        return cleaned_data


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = CustomUser
        fields = '__all__'


class CustomUserChangeForm(UserChangeForm):
    class Meta(UserChangeForm):
        fields = ('email', 'first_name', 'last_name', 'role', 'is_active', 'is_admin', 'password')
