from django.urls import path, include
from .views import create_horse, view_horse, edit_horse, delete_horse, horse_details

app_name = 'horses_url'

urlpatterns = [
    path('create-horse/', create_horse, name='create_horse'),
    path('horses/', view_horse, name='view_horse'),
    path('edit-horse/<int:horse_id>', edit_horse),
    path('delete-horse/<int:horse_id>', delete_horse),
    path('horse-details/<int:horse_id>', horse_details)
]