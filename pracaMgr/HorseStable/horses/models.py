from django.core.validators import MaxValueValidator
from django.db import models
from suplements.models import Suplements
from django.utils.translation import ugettext_lazy as _
from HorseStable.settings import AUTH_USER_MODEL
# Create your models here.
IMAGES_PATH = 'horses/static/images'


class Horses(models.Model):
    MALE = 'Ogier'
    FEMALE = 'Klacz'
    GELDING = 'Wałach'
    SEX_CHOICES = (
        (MALE, _('Ogier')),
        (FEMALE, _('Klacz')),
        (GELDING, _('Wałach'))
    )
    age = models.PositiveSmallIntegerField(
        blank=False,
        null=False,
        validators=[MaxValueValidator(30, message=_('You can enter age below 30 years'))]
    )
    user = models.ForeignKey(AUTH_USER_MODEL, on_delete=models.CASCADE, blank=True, related_name='horse')
    name = models.CharField(max_length=255, blank=False, null=False)
    breed = models.CharField(max_length=50, blank=True, null=True)
    sex = models.CharField(choices=SEX_CHOICES, max_length=7, default=MALE)
    mother_name = models.CharField(max_length=255, blank=True, null=True)
    father_name = models.CharField(max_length=255, blank=True, null=True)
    photo = models.ImageField(upload_to=IMAGES_PATH, blank=True, null=True)
    suplement = models.ManyToManyField(
        Suplements,
        blank=True,
        related_name='suplements'
    )

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "horse"
        verbose_name_plural = "horses"
