from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect

from health_card.models import HealthCard
from .forms import HorseCreationForm, HorseEditForm
from .models import Horses
from django.http import HttpResponse
from django.utils.translation import ugettext_lazy as _
from django.template.defaulttags import register


@login_required
def view_horse(request):
    horses = Horses.objects.filter(user=request.user)
    return render(request, 'crud/read.html', {'horses': horses})


@register.filter
def get_item(dictionary, key):
    return dictionary.get(key)


@login_required
def horse_details(request, horse_id):
    horse_id = int(horse_id)
    try:
        horse = Horses.objects.get(id=horse_id)
    except Horses.DoesNotExist:
        return HttpResponse(_('Koń nie istnieje'))
    horse_dict = dict()
    for field, value in horse.__dict__.items():
        if field.startswith('_') or field == 'id' or field == 'user_id':
            continue
        horse_dict[field] = value
    suplements = horse.suplement.all
    schedule_items = horse.schedule_item.all
    try:
        health_card = HealthCard.objects.get(horse=horse_id)
    except:
        health_card = "Brak"
    image_path = str(horse.photo)[14:len(horse.photo)] if horse.photo else "None"
    return render(request, 'horse_details.html', {
        'horse': horse,
        'health_card': health_card,
        'suplements': suplements,
        'schedule_items': schedule_items,
        'photo_path': image_path})


@login_required
def create_horse(request):
    form = HorseCreationForm()
    if request.method == 'POST':
        form = HorseCreationForm(request.POST or None)
        if form.is_valid():
            horse_obj = form.save(commit=False)
            horse_obj.user = request.user
            form.save()
            return redirect('horses_url:view_horse')
        else:
            return render(request, 'crud/create.html', {'form': form})
    else:
        return render(request, 'crud/create.html', {'form': form})


@login_required
def edit_horse(request, horse_id):
    horse_id = int(horse_id)
    try:
        horse = Horses.objects.get(id=horse_id)
    except Horses.DoesNotExist:
        return HttpResponse('Koń nie istnieje')
    form = HorseEditForm(request.POST or None, instance=horse)
    if form.is_valid():
        form.save()
        return redirect('horses_url:view_horse')
    return render(request, 'crud/edit.html',
                  {'form': form, 'horse_id': horse.id, 'horse_schedule_item': horse.schedule_item.all()})


@login_required
def delete_horse(request, horse_id):
    horse_id = int(horse_id)
    try:
        horse = Horses.objects.get(id=horse_id)
    except Horses.DoesNotExist:
        return redirect('horses_url:view_horse')
    horse.delete()
    return redirect('horses_url:view_horse')
