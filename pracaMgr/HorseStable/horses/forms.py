from django import forms
from horses.models import Horses


class HorseCreationForm(forms.ModelForm):
    class Meta:
        model = Horses
        fields = '__all__'
        widgets = {
            'suplement': forms.widgets.CheckboxSelectMultiple,
            'user': forms.widgets.HiddenInput
        }
        exclude = {'schedule_item'}


class HorseEditForm(forms.ModelForm):
    class Meta:
        model = Horses
        fields = '__all__'
        widgets = {
            'schedule_item': forms.widgets.CheckboxSelectMultiple,
            'suplement': forms.widgets.CheckboxSelectMultiple
        }
        exclude = {'user'}
