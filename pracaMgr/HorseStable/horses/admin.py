from django.contrib import admin
from horses.models import Horses
from horses.forms import HorseCreationForm


# Register your models here.
class HorseCreationAdmin(admin.ModelAdmin):
    form = HorseCreationForm


admin.site.register(Horses, HorseCreationAdmin)
