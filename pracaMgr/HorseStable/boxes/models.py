from django.db import models
from horses.models import Horses


class Box(models.Model):
    isBorrowed = models.BooleanField(default=False)
    horse = models.OneToOneField(
        Horses,
        on_delete=models.CASCADE,
        related_name='horse'
    )

    def __str__(self):
        return self.horse.name

    class Meta:
        verbose_name = "box"
        verbose_name_plural = "boxes"